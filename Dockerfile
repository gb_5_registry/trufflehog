FROM python:3-alpine

#add curl
RUN apk add --no-cache git curl && pip install gitdb2==3.0.0 trufflehog
RUN addgroup -S ibuser && adduser -S -G ibuser ibuser

#using a universal docker user
USER ibuser

#use an empty entrypoint

ENTRYPOINT [ "" ]
WORKDIR /proj
